import java.util.Arrays;
import java.util.Scanner;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;


public class Streams{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		Arrays.stream(requireNonNull(str, "Can't be null").toLowerCase().split("\\PL+"))
			.collect(groupingBy(s -> s, counting()))
			.entrySet().stream()
			.sorted((word, count) -> {
				int res = count.getValue().compareTo(word.getValue());
				return res == 0 ? word.getKey().compareTo(count.getKey()) : res;
			})
			.limit(10).forEach(System.out::println);
	}
}